# KeePass pour le second facteur

L'avantage des bases de données de KeePass, c'est que ce sont des bases de données. On peut donc mettre toutes sortes de choses dedans qui ne soient pas des mots de passe. Nous allons voir comment nous pouvons conserver des "jetons" (token) qui servent à la double authentification.

## La double authentification en pratique, comment se place KeePass

Pour une introduction sur ce qu'est la double authentification, voir le fichier Double_authentification.md

Il y a plusieurs façons de s'authentifier avec la double authentification. Rappellons les différentes manières (classées par ordre de sécurité croissante) :

   - L'envoi d'un code par SMS qui doit être saisi à la connection (le téléphone est le second facteur)
   - La combinaison entre un jeton logiciel ou "software token" et la date (le token est le second facteur, j'ai détaillé le fonctionnement dans mon tutoriel Double-authentification.md)
   - Le jeton matériel ("hardware token") est un appareil qui recréé la combinaison précédente et l'affiche sur un écran (ce système est rare sur internet, Paypal est le seul site qui en propose)
   - Le challenge cryptographique public-privé est la technique utilisé dans les badges d'accès ou le protocole SSH mais n'a malheuresement jamais fait son chemin jusqu'au web (en HTTPS il y a bien implication de clé publique et privée mais ce n'est pas pour l'authentification).
   - Enfin le plus fort pour la navigation web est le U2F ou FIDO2, cette authentification est résistante au phishing contrairement à toutes les autres authentifications. Nécéssite une clé USB particulière ou authentificateur compatible.

KeePass vous permet d'utiliser plus facilement la méthode du jeton logiciel (voire matériel qui peut être retro-ingénrié dans le cas de PayPal). Le principe est simple : vous sauvegardez le jeton dans la base de donnée comme un champs additionnel au mot de passe, KeePass va directement calculer pour vous le code de connection quand vous en aurez besoin.

## Le plugin KeeOTP ou KeeTrayTOTP

J'utilise couramment KeeOTP, c'est donc le comportement de ce module que je vais décrire mais je ne doute pas que mon tutoriel s'applique à KeeTrayTOTP.

Premièrement, ces deux plugins se retrouvent facilement sur la page https://keepass.info/plugins.html .

/!\ Rappel de sécurité : Le code source des plugins n'a pas été audité par le concepteur de KeePass ni l'ANSSI ni l'UE ! Il y a donc un danger qu'ils ne soient pas totalement de confiance ! Ils pourraient installer une porte dérobée à votre insu pour envoyer des informations par internet ! Cependant, les plugins ne pourrons jamais récupérer le mot de passe de votre base de donnée et beaucoup de gens les utilisent sans que personne ne remarquent quoique ce soit d'anormal, le code source est bien sûr consultable en ligne et personne n'a rien trouvé à en dire. Gardez bien en tête que vous vous exposez toujours à un danger quand aucun tier de confiance ne garanti que le programme que vous installez est sûr. Le risque est ici très faible (car il y a très peu de chance que le danger soit là), il faudrait voir le mal partout pour refuser de les utiliser mais vous voilà prévenu !

Une fois installé et KeePass redémarré, une option supplémentaire au clique droit sur une entrée apparaît dans le menu : "Timed One Time Password" ou TOTP pour les intimes.

## Ajouter le second facteur

Google est certainement le meilleur site en terme de clarté pour la sécurité du compte. Je vais donc m'appuyer sur Google pour vous dire quoi chercher sur un site qui supporte la double authentification.

   - Aller sur la page de votre compte Google sur https://myaccount.google.com/ puis aller sur l'onglet "Sécurité" (menu à gauche). 
   - Aller sur la partie "Se connecter à Google" et cliquer sur "Double-authentification"
   - Google vous donne quelques informations et vous propose d'activer la double-authentification, confirmez

Google est une référence en matière de sécurité informatique. Il va donc vous proposer une liste de 10 TAN (Transaction authentication number). Les TANs sont des codes à usage unique qui serviront au cas où vous n'arrivez plus à vous connecter car vous avez perdu le jeton logiciel, le mot de passe sera en revanche toujours requis. Vous pourrez vous connecter avec cette méthode de dernier recours 10 fois (un code différent à chaque fois) pour recréer un jeton logiciel. Google n'est pas le seul site à vous proposer des TANs en dernier recours de la double authentification mais tous ne sont pas si exemplaire (ce n'est pas le cas de Facebook ou Twitter... herm). Imprimez les, conservez les dans un endroit connu pour conserver des informations personnelles papier, inutile d'en conserver un fichier sur l'ordinateur (il faudrait le faire dans un environnement chiffré et que le code d'accès soit fort donc géré par KeePass, or si vous n'arrivez plus à vous connecter c'est surement parce que vous n'arriverez plus à ouvrir votre base de donnée... bref ils ne servent à rien sur l'ordinateur et vous expose à un vol).
Le suite des évènements :
   - Google vous montre un QRcode et vous demande de le flasher avec l'application Google Authenticator sur votre téléphone, vous pouvez le faire si vous le souhaitez
   - (dans le cas où vous avez flash le QR code avec votre téléphone et que le code temporaire à 6 chiffre s'affiche sur votre téléphone, NE PAS RENTRER LE CODE TEMPORAIRE TOUT DE SUITE SUR L'ORDINATEUR)
   - Cliquer sur "Impossible de scanner le code ?"
   - Le QR code disparaît et laisse place à une chaine de 16 caractères, copier la
   - Clique droit sur l'entrée de votre compte Google sur KeePass, choisir l'option "Timed One Time Password"
   - Copier la chaîne de caractère précédente dans le champs textuel.
   - Un code de 6 chiffre doit s'afficher (remarque : il doît être identique à celui que vous affiche Google Authenticator sur le téléphone si vous avez falshé le code barre). Revenez sur la fenêtre de gestion de votre compte google et demander la vérification
   - Saisir le code de 6 chiffres que vous venez de voir.
   - Si tout s'est bien passé, Google vous indique que la double authentification est activée. Sinon, vous avez dû mal saisir la chaîne de caractère dans KeePass, ou alors vous avez joué sur les paramètre de KeeOTP vérifier que KeeOTP créé es codes à 6 chiffres basé sur l'horloge toutes les 30 secondes (vérifiez aussi que l'heure affichée sur votre ordinateur est la bonne !)

La procédure est plus ou moins la même sur tous les site accéptant ce genre d'authentification. Cependant certains sites sont assez... agaçants ? Ils refuseront de vous délivrer ce genre de service si vous n'activez pas l'authentification avec le téléphone avant (ce qui vous oblige à donner un numéro de téléphone, merci Twitter, c'est toi qu'on regarde). A vous de décider dans ces cas là.

## Se connecter avec la double-authentification

Pour tester la double authentification, retournez sur la page de gestion de la double authentification (dans la partie "Securité") aller sur "appareils fiables" et cliqué sur "Tout annuler" puis déconnectez vous de votre compte Google. 

Pour vous reconnectez, Google vous demande l'adresse mail de votre compte et votre mot de passe (que vous coller depuis KeePass).

Vient ensuite la phase de double authentification, qui est bien plus simple :
   - Clique droit sur l'entrée google dans KeePass
   - Afficher le "Timed One Time Password" qui est un code à 6 chiffres qui change toutes les 30 secondes, il est identique à celui donnée par Google Authenticator
   - Saisir les 6 chiffres affichés dans le champs à remplir sur Google
   - Vous êtes connecté !

Un compteur vous indique aussi le temps avant qu'un nouveau code soit généré et affiché à la place (un code s'affiche 30 secondes), cependant la fenêtre de temps de validité est généralement plus large, vous avez le temps de saisir et valider le code précédent celui qui s'affiche même en cas de mauvais timing.

Attention : les sites aiment vous proposer d'ajouter l'appareil comme un "appareil fiable", ainsi plus besoin de rentrer le code TOTP à la prochaine connection. Je ne pense pas que ça soit une bonne idée car il me semble trop facile d'usurper un appareil et celà introduit donc un moyen détourné de ne pas saisir le code TOTP sur l'ordinateur d'un pirate qui aurait trouvé votre mot de passe. La double authentification perd de son intérêt (il reste cependant plus difficile pour un pirate d'accéder à votre compte). Pour la démonstration, je vous ai justement fait supprimer tous les appareils fiables pour pouvoir vous montrer comment la connection se déroule. De manière général, il me semble plus sécurisé de supprimer tous les cookies de session et ne pas enregistrer les navigateurs pour ne pas introduire de porte dérobé. 

## Complétion du champs TOTP automatique avec Kee

Comme la double authentification demande de saisir un code temporaire, il est très avantageux d'avoir des outils pour remplir automatiquement le champs sur le navigateur. Nous allons voir comment faire avec Kee.

A suivre ...
