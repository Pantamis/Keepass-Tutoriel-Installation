## La double authentification, qu'est ce que c'est et pourquoi ?

J'explique ici ce qu'est selon moi la double-authentification, pourquoi c'est important et comment la mettre en place facilement avec KeePass. Si vous souhaitez passer directement à l'utilisation du double facteur avec KeePass, voir le tutoriel KeePass_TOTP.

# 1. Identification et Authentification

Wikipédia propose une bonne introduction de l'authentification forte mais je vous présente ici ma vision.

Il faut d'abord distinguer identification et authentification :

   - L'identification désigne l'action consistant à donner un nom ou code à quelque chose
   - L'authentification est un processus permettant de s'assurer de la légitimité de la demande

L'identification en informatique est donc donner son nom, vous présenter (en tant que personne, programme ou fichier). L'authentification est prouver une légitimité d'accès ou de pouvoir liée à la personne que vous prétendez être. L'authentification est donc une notion juridique, c'est la preuve de la légitimité de votre action par un témoin qui fait le lien entre vous et les droits auquels vous prétendez : elle nécessite donc intrinsequement un tier de confiance !

L'authentification implique ainsi trois entités : vous, ce à quoi vous demander des droits et le tiers de confiance. 

Par exemple, pour accéder à vos compte en ligne vous devez donner votre identité via votre login et vous vous authentifiez via votre mot de passe. Ainsi les trois entités de l'authentification sont :

   - Vous via le login qui permet de vous présenter au site de manière fiable et unique
   - Votre compte sur le site qui vous demande à vous authentifier pour accéder à votre compte
   - Le site (tier de confiance) qui conserve en plus de votre login un mot de passe (que vous n'êtes pas forcément le seul à utiliser !) pour permettre de vous authentifier

Mais il existe d'autres manières de s'authentifier que le mot de passe !

# 2. Les facteurs d'authentification

Un facteur d'authentification est une empreinte que vous laisser au tiers de confiance lorsque vous obtenez des droits pour permettre à celui-ci de vous authentifier. 

Pour reprendre Wikipédia sur la page facteur d'authentification, il y en a 4 :

Facteur mémoriel (ce qu'il sait)
   - Empreinte : une information qu'il a mémorisé.
   - Exemples : le nom de sa mère ou un mot de passe.

Facteur matériel (ce qu'il possède)
   - Empreinte : une information contenue dans un objet qu'il utilise.
   - Exemples : une clé USB, un identifiant sur bande magnétique, un certificat numérique sur une carte à puce.

Facteur corporel (ce qu'il montre)
   - Empreinte : une trace corporelle qu'il peut laisser quelque part.
   - Exemples : une empreinte digitale, les caractéristiques de sa pupille, sa voix.

Facteur réactionnel (ce qu'il fait)
   - Empreinte : un geste qu'il peut reproduire.
   - Exemples : sa signature.

# 3. L'authentification forte

Il y a une question naturelle qui vient à toute personne qui souhaite se protéger : "N'en fais-je pas trop ? Cette protection supplémentaire est-elle utile ?"
Les 4 facteurs d'authentification permettent de mieux répondre à cette question.

En général, vous n'utilisez qu'un seul facteur pour vous authentifier sur un compte en ligne : le mot de passe. Vous n'utilisez donc que le facteur mémoriel. 

Le principe est simple : une protection supplémentaire n'en vaut la peine que si elle fait appel à un facteur d'authentification encore non utilisé ! (conserver un fichier chiffré par mot de passe dans un conteneur chiffré aussi par mot de passe n'apporte pas plus de sécurité qu'un mot de passe plus fort comme la concaténation de ces mots de passe)

Lorsque deux facteurs DIFFÉRENTS sont utilisés, on parle alors d'authentification forte car on peut maintenant faire un lien entre plusieurs empreintes indépendantes ce qui réduit fortement les chances que vous ne soyez pas la bonne personne (ou alors vous liez ces informations en les laissant traîner en public par négligence).

Enfin une remarque, il n'y a pas de facteur d'authentification meilleur ques les autres, un usurpateur peut :

   - deviner le facteur mémoriel (casser le mot de passe)
   - voler ou dupliquer le facteur matériel  (capter le fichier qui permet l'authentification, double de clé)
   - Répliquer le facteur corporel (empreinte digital sur écran de téléphone, photo de l'iris ou visage, enregistrement voix)
   - Apprendre ou copier le facteur réactionnel (imitation de signature)

Le pari est qu'il est beaucoup plus dur de disposer de plusieurs facteurs que d'un (un spécialiste du cassage de mot de passe n'est pas forcément un bon voleur ou imitateur), c'est bien pour ça qu'on parle d'authentification forte et en particulier de double facteur.

# 4. Le second facteur, cas classique

Le second facteur proposé par les sites est le facteur matériel. 

Souvent, les sites se limitent à une validation par SMS de la connection. Après validation du mot de passe (facteur mémoriel), un code temporaire est envoyé sur le numéro de téléphone associé au compte par SMS et vous le saisissez sur le site. Comme le code est temporaire, il ne s'agit pas d'un facteur mémoriel et c'est donc le téléphone qui sert de facteur matériel en complément du mot de passe. Il y a donc bien double authentification.

Cependant, je ne recommande pas cette méthode car elle est assez peu fiable : il n'est pas si difficile de récuperer vos SMS lorsqu'on connait votre numéro de téléphone (qui est connu au moins par vos amis). La meilleure méthode est à mon avis le "software token". Le principe :
   - Le site vous communique et conserve de son côté une chaîne de caractères comme clé (aussi appellé "token") qui doit rester secrète 
   - Lors de la connection, après saisie du mot de passe, une application (ou un plugin KeePass) combine cette clé avec la date à 30 secondes près
   - Sur le résultat obtenu, on applique une transformation qui est extrèmement sensible à la moindre modification
   - On déduit du résultat un arrangement de chiffres (6 généralement) qu'on saisie à la demande du site
   - Le site vérifit qu'il obtient bien le même code en suivant le même protocole, et débloque l'accès au compte s'il y a correspondance.

En résumé : la clé vous permet de créer un code avec une application générique qui n'est valable qu'une minute pour vous connecter. Le "token" est donc le facteur matériel, il faut donc le conserver secrètement car étant un simple fichier, il est facile de le dupliquer, ce qui permet aussi de réaliser facilement des sauvegardes. Vous pouvez utiliser des archives chiffrées en 7zip par mo de passe ou le conteneur chifré d'une application dédiée (comme Google Authenticator) pour les conserver de manière sécurisé. Vous pouvez aussi utiliser .... une base de donnée KeePass bien sûr ! (et nous allons voir pourquoi c'est beaucoup plus pratique)

Remarque : Lorsque vous activez la double authentification, le site doit bien vous communiquer la clé. Cette étape est CRITIQUE car c'est le SEUL MOMENT où la clé peut être interceptée, il faut absolument s'assurer que la connection avec le site est la plus étanche possible (https) car l'interception de cette clé par un pirate lui permet de générer les bons codes de connections ! Si cette étape s'est bien déroulée, vous n'enverrez jamais cette clé mais que des codes obtenues par elle (dont il est quasi-impossible d'en déduire la clé). La validité de ces codes est d'une minute, si un pirate connaît votre mot de passe et peut intercepter les communications avec le site, il peut tenter de récuperer le code temporaire et se connecter avec dans le cours laps de temps. Il est possible que le site vous protège en n'autorisant pas la connection deux fois de suite avec le mêême code, mais vous ne savez pas, la double authentification ne vous dispense pas de prudence notamment vis-à-vis du hammonçenage.

# 5. Double authentification et KeePass

Il existe deux plugins KeePass pour gérer les doubles authentifications : KeeOTP et KeeTrayTOTP. Dans le tutoriel KeePass_TOTP je vous explique comment utiliser KeeOTP.