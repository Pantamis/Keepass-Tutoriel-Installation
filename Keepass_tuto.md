## Installer et utiliser Keepass

# 1. Où et quoi ?
Pour installer rendez-vous sur la page : https://keepass.info/.

/!\ Attention le site keepass.fr est un site d'hamonçennage !
Vous pourrez installer une traduction de Keepass en suivant le lien https://keepass.info/translations.html.

# 2. Quelles versions ?

Sur la page https://keepass.info/download.html vous trouverez deux versions téléchargeable. 
Je vous recommande la version 2 comptible avec tous les OSs et une belle application Android nommé Keepass2Android.

Choisir "Installer" pour l'installer sur un ordinateur et "Portable" si vous souhaitez créer une clé USB en standalone.
Vous aurez besoin de retrouver le répertoire d'installation de KeePass2 (généralement ProgrammsFile sur windows 
et /usr/lib/keepass2 sur Linux)

J'ai personnellement une préférence pour les versions portables car vous pourrez ajouter les plugins sans droit administrateur.

# 3. Installation Linux

La version portable est utilisable telle qu'elle sur linux. Pour la lancer, utiliser la commande mono.

Si vous tenez à utiliser un précompilé : vous pourrez télécharger un .deb pour un OS basé Debian à la page : 
https://packages.debian.org/sid/utils/keepass2

Bien que ce packet propose d'installer les libraries mono nécessaires pour un fonctionnement minimaliste, 
il faut installer les lib mono complètes pour que les plugins fonctionnent bien sur Linux 
notamment les plugins de liaison avec les navigateurs. 

Pour celà, il faut utiliser la commande suivant sur Ubuntu :
sudo apt install mono-complete

Bien que basé Windows, KeePass2 fonctionne parfaitement sur Linux avec mono, son créateur porte une attention toute particulière à ce que toutes les versions de KeePass soit utilisable sur Linux via mono, cependant certaines fonctions non-essentielles seront indisponibles. 

Il peut être souhaitable de se tourner vers une alternative à KeePass2 codée nativement pour Linux, KeePassXC est une solution communautaire parfaitement compatible avec les fichiers KeePass2. Cependant, celle-ci ne dispose pas d'environnement de plugins, beaucoup de fonctionnalités personnalisés sont inaccessibles, le logiciel est cependant plus complet.

# 4. Au lancement

Lors du premier lancement, KeePass2 vous proposera de lier les extensions de fichier .kdbx à KeePass2 
(l'OS les ouvrira par défaut avec KeePass2 si vous acceptez). 

Les fichiers .kdbx créés et gérés par KeePass2 sont des fichiers bases de données. Ils sont chiffrés, 
KeePass2 vous demandera donc le mot de passe maître du fichier à la création et à l'ouverture.

A la création d'un fichier .kdbx, KeePass2 vous fera définir un mot de passe et vous l'évaluera (score entropique, 
le nombre de tentatives à faire pour trouver le mot de passe par force brute et dictionnaire est au plus
2 puissance le nombre de bits indiqués, la force maximum atteignable avec le chiffrement AES-256 est 256 bits). 

Il n'y a aucun restriction par KeePass2 sur la qualité du mot de passe maître mais il doit être fort :
en effet le fichier qu'il protège contient tous vos autres mots de passe en clair. Vos autres mots de passe,
aussi forts soient-ils, deviennent faibles si le mot de passe maître est faible. En revanche, il faut toujours 
pouvoir le retrouver car il n'existe aucun autre moyen de le retrouver que de fouiller votre mémoire jusqu'à épuisement 
de vos idées. En cas d'oubli du mot de passe, KeePassjohn permet de tester rapidement plein de combinaisons, mais vous devez vous rappeller et spécifier des règles sur les différents possibilités les plus précise et exact possible : il est impossible de tout tester, c'est la dur loi du chiffrement et c'est ce qui vous protège.


Conseil pour créer un mot de passe fort facile à retenir : https://xkcd.com/936/
Préférez donc les mots longs sans rapport les uns aux autres pour produire un mot de passe fort facile à retenir. Choisir des mots aléatoirement n'est pas forcément simple, si vous souhaiter des garanties DiceWare est une manière rigoureuse de creer un mot de passe dont on peut garantir la force.

Je recommande de n'utiliser la méthode du fichier clé qu'accompagné d'un mot de passe fort. Le fichier clé est un fichier quelconque ou dédié sur l'ordinateur qui vient compléter le mot de passe de la base de donnée. Attention : il est trop facile de corrompre un fichier s'il a une quelconque utilité, la moindre modification de celui-ci rend la clé complète invalide (même un changement de nom !). Le fichier clé est surtout utile si vous déposez votre fichier base de donnée sur le cloud pour renforcer la clé de chiffrement afin que le service cloud ne puisse jamais décrypter la base de donnée (ceci suppose que le fichier clé ne soit en revanche pas déposé sur le cloud). Il existe bien des solutions pour chiffrer le fichier clé lui-même efficacement et atteindre un idéal de sécurité maximale, mais pour apporter une vrai protection supplémentaire il faut utiliser du matériel informatique adapté. Les smartcards ou USB d'athentification comme les YubiKey sont des mots clés qui pourront intéresser celui qui souhaite une base de données au chiffrement inviolable.

# 5. Installer une traduction ou des plugins

* Depuis le répertoire d'installation :
    * Télécharger French.lngx, à la page https://keepass.info/translations.html (choisir la bonne version)
    * Dans le répertoire Languages, déposer le fichier French.lngx

* Pour les plugins :
    * Télécharger le plugin voulu de préférence depuis la page https://keepass.info/plugins.html
    * Déplacer le contenu dans le répertoir Plugins dans le répertoire d'installation de KeePass2

# 6. Cas pratique sans Kee

La base de donnée est un mini-systeme de fichier. Des répertoires ont déjà été crées par KeePass2 dans votre base de données. Clic droit dans la fenêtre principale, vous pouvez créer une entrée. Vous donnez un titre, un login, le mot de passe et l'URL du site qui le concerne. 
Vous pouvez également ajouter une date d'expiration à votre entrée pour vous forcer à changer régulièrement vos mots de passe.

Vos mots de passe mails sont les plus sensibles (le mail permet en général de récupérer son compte en cas d'oubli) et sont généralement négligés, je vous recommande de les changer pour des mots de passe très forts rapidement.
Une fois que vous avez complété votre base de données avec quelques mots de passe, selectionnez l'entrée qui vous intéressent :

     * Pour copier le nom d'utilisateur sauvegardé dans l'entrée : Ctrl + B  
     * Pour copier le mot de passe sauvegardé dans l'entrée : Ctrl + C 
     * Pour copier le mot de passe temporaire TOTP (utile si vous utilisez la 2FA, voir les plugins conseillés)  : Ctl + T  
     * Modifier l'entrée : Retour chariot (Entrée)

Les commandes de copie écrivent dans le presse-papier, mais celui-ci est éffacé au bout de 12 secondes pour éviter les étourderies (coller son mot de passe dans la barre de recherche google le compromet si la recherche anticipée est activée). Cette durée est réglable dans les paramètres du logiciel KeePass. 

Vous pouvez également activer le système d'obfuscation à double canaux pour les entrées que vous désirez. Ceci permet de se protéger d'un éventuel programme malveillant qui journaliserait le contenu du presse-papier. Notez que la base de donnée est chargée en mémoire RAM quand elle est ouverte. Pour éviter un usage non-désiré par une autre personne qui peut acceder à l'ordinateur durant vos sessions, vous devez fermez la base de données pour forcer la saisie du mot de passe maître. N'oubliez pas de sauvegarder les changements éventuels ! (la base de données reste un fichier avant d'être un répertoire)

Malgré ces protections, utiliser KeePass sur un ordinateur corrompu reste extrèmement risqué /!\ : un programme malveillant peut enregistrer toute votre activité et ainsi récupérer votre mot de passe maître ! KeePass tente de vous protéger en chiffrant le contenu en RAM, en proposant un bureau sécurisé pour taper votre mot de passe maître (Windows uniquement), en permettant l'obfuscation à double canaux.... Mais ce ne sont des protections que contre des logiciels malveillants génériques, si le pirate s'intéresse aux données récoltés par ses programmes sur votre activité il pourra à la main reconstituer les mots de passe, ça sera pénible mais s'il a une bonne raison il pourra le faire sans encombre (le niveau sécurité n'est plus ou moins qu'une question de coût pour vous protéger versus coût pour vous attaquer, où coût peut désigner le temps et/ou l'argent).

# 7. Ne pas réfléchir au choix du mot de passe : le générateur

Il est possible de demander à KeePass de générer aléatoirement le mot de passe d'une entrée ! 
La petite clé vous propose de choisir un profil de mot de passe aléatoire. KeePass vous propose quelques profils de génération de mots de passe aléatoires mais il est bon de créer les vôtres. Pour celà, rendez vous dans le générateur de mots de passe dans KeePass (outils, générer un nouveau mot de passe).
Choisissez le nombre de charactères, les différents types (maj, min, chiffres, paranthèses, espace ....) et cliquer sur la disquette pour enregistrer et donner un nom clair à ce profil de mot de passe (précisez soit pour quelles entrée il est adapté soit le choix de types)

Vous pouvez ensuite générer un échantillon en appuyant sur la clé à côté du mot de passe quand vous éditer l'entrée KeePass. Un bon mot de passe généré aléatoirement est par définition impossible à retenir mais il exploite toute la force combinatoire du chiffrement (c'est bien le dernier type qu'essaira le pirate voulant cracker le mot de passe). Mais pas de problème vous avez un gestionnaire de mot de passe !


/!\ Attention : il s'agit ici de changer les mot de passe enregistré dans le logiciel seulement, nous verrons dans Kee comment on peut changer les mots de passe sur les sites facilement ! Les options de génération décrites sont en fait rarement utiles mais elles se combinent très bien avec Kee (voir le tuto dédié)


