## Keepass-Tutoriel-Installation

* Keepass est un gestionnaire de mots de passe. Il est certifié par l'ANSSI depuis 2013 et recommandé par la CNIL : 
    * https://www.youtube.com/watch?v=XTnDKJl1zOQ
    * https://www.ssi.gouv.fr/entreprise/certification_cspn/keepass-version-2-10-portable/
    * https://www.cnil.fr/fr/les-conseils-de-la-cnil-pour-un-bon-mot-de-passe
    
Keepass est open-source, donc gratuit, compatible Windows Linux et Mac. Il est disponible aussi sur Android 
et bien qu'il soit installé en standalone il existe des plugins permettant de l'intégrer aux navigateur et 
particulièrement Firefox via Kee (plugin téléchargeable sur le site Mozilla).

# 1. Principe de base :

Un mot de passe pour les retenir tous !
Keepass permet de créer et gérer des bases de données chiffrées contenant des codes d'accès. 
Un mot de passe est donc nécessaire pour ouvrir la base de données (chiffrés AES-256 par défaut, un standard de chiffrement très sûr).
Il sera imposible de retrouver les mots de passe contenus dans le fichier sans ce mot de passe maître, ne le perdez pas !

Vous savez peut être que les navigateurs proposent une telle possibilité mais l'algorithme de chiffrement est assez faible 
(des logiciels permettent de cracker le mot de passe de manière assez fiable) et peut être n'accorderiez vous pas 
une confiance infaillible à votre navigateur pour retenir vos mots de passe sans risque de les divulger. Enfin notez que vous avez besoin de vos mots de passe partout et qu'il est plus pratique de pouvoir les gérer avec d'autres programmes qu'un navigateur.

# 2. Pourquoi l'installer ? J'ai pas confiance dans les gestionnaire de mots de passe ...


KeePass est un gestionnaire de mot de passe open source certifié OSI (il est donc bien maintenu par le créateur et la communauté) et certifié par l'ANSSI (Agence Nationale de la Sécurité des Systèmes d'Information). Cette certification (dites de sécurité de premier niveau) assure que la version du logiciel testé (la 2.10 pour KeePass, mais le logiciel n'a pas changé sensiblement sur son système de sécurité) est conforme à ce que prétend assurer le logiciel en terme de sécurité. D'autres part, l'Union européenne à choisit d'inclure KeePass dans un programme de financement de "Bug Bounty", si une faille est repéré par un expert dans la sécurité de KeePass, il peut être rémunéré par l'Europe pour la découverte de la faille. Donc c'est du sérieux ! Le fichier contenant vos mots de passe est très bien sécurisé si vous choississez un mot de passe maître fort.

Une bonne raison d'utiliser ce type de logiciel est la suivante : https://xkcd.com/792/. La réutilisation chronique des mêmes mots de passe est dangereuse car en cas de fuite de vos mots de passe sur des sites mêmes anecdotiques, on peut réussir à deviner comment vous formez vos mots de passe et donc prendre possession de tous vos comptes pour usurper votre identité et/ou vous volez (il suffit notamment de prendre possession du compte mail utilisé en récupération de vos mots de passe). Vous pouvez vérifier que vos comptes ne sont pas compromis sur le site https://haveibeenpwned.com/ en tapant l'adresse mail de login de ceux-ci dans le champs principal. KeePass dispose d'un générateur de mots de passe aléatoires. Les mots de passe générés aléatoirement sont de TRÈS LOIN les PLUS FORTS, et pour cause le fait d'être généré de cette façon a pour conséquence que :

   - Même si par inadvertence ils s'afficheraient en clair temporairement (comme sur les smartphones par un copier-coller) une personne qui regarderait l'écran aurait une (très) grande difficulté à les lire et donc les mémoriser
   
   - Ils imposent aux potentiels hackeurs de vous attaquer par pure brute-force, aucune technique de dictionnaire ou social engeneering ne permet d'éventuellement gagner du temps. Celà a pour conséquence que :
   
   - Ils seront forcément les derniers mots de passe essayés par un éventuel hackeur tenace, ils sont donc les PLUS FORT DE TOUS. A ce niveau là, si vos mots de passe sont assez longs, ce n'est plus un individu qui attaque vos mots de passe mais une organisation (entreprise ou État...)
   
   - Ils évitent de tomber dans l'usage le plus déconseilé des mots de passe : la réutilisation. La réutilisation d'un mot de passe ou de variantes de celui-ci sur plusieurs sites rendent tous ces mots de passe faibles dés lors que deux sites sur lesquelles vous êtes inscrits ont vu leurs identifiants et mots de passe fuiter (on peut alors identifier les éléments communs à ses mots de passe et la façon de créer les variantes). Les mots de passe générés aléatoirement ne sont PAS REPRODUCTIBLES, chaque site à un mot de passe très différent, risque 0.
   
   - Vous dépendez en revanche de KeePass pour retrouver vos mots de passe (n'espérez pas retrouver de tête des mots de passe que vous n'avez pas choisi et que vous ne saisirez jamais). Il faudra donc créer dupliquer et synchroniser les fichiers de base de données chiffrée pour disposer de sauvegardes accessibles partout tant que vos mots de passe sont générés ainsi.

Enfin, pourquoi KeePass plus qu'un autre ? En 2017, LastPass, une entreprise proposant un gestionnaire de mot de passe, a été hackée ! L'entreprise promet que les mots de passe de ces utilisateurs n'ont pas fuité mais personne ne peut vérifier puisqu'on n'a pas le code source (logiciel propriétaire) !! Vous avez compris : avec KeePass vous avez un logiciel que des pointures peuvent vérifier qu'il fait le job. Vous avez le controle de votre base de donnée chiffrée, vous la savez bien sécurisée, vous avez votre sécurité en main. KeePass est le seul logiciel libre dont le niveau de sécurité est officiellement éprouvé et approuvé.

# 3. Ce que n'est pas KeePass


KeePass ne propose pas de :
   - Protéger vos mots de passe alors que le mot de passe maître est faible (c'est même pire : tous vos mots de passe deviennent facilement accessibles /!\ )

   - Retrouver vos mots de passe alors que vous avez oublié le mot de passe maître (vous trouverez des conseils pour bien le choisir ici)

   - Retrouver vos mots de passe alors que vous avez ni retrouvé le fichier qui les contient ni programmé de sauvegarde régulière de celui-ci (mais KeePass permet de les programmer !)

   - Vous protéger d'une attaque rejouée si vous n'utilisez pas la 2FA (une attaque rejouée est la captation puis utilisation ultérieur d'un mot de passe pendant la connection voir le tuto dédié)

   - Ni de vous protéger du phishing

   - Protéger vos mots de passe de toutes les attaques informatiques notamment de programmes malveillants que vous avez installé (les options de sécurité proposées par KeePass ne sauraient vous prévenir indéfiniment de votre turpitude)

   - Garantir un niveau de sécurité de ses plugins équivalent à son standard, ceux-ci étant libre et non évalués par l'ANSSI ou l'UE

   - Être aussi pratique que lorsqu'on n'utilise pas de gestionnaire de mots de passe (il fait ce qu'il peut déjà et il peut être quasi-transparent)

   - Être un beau logiciel avec un design moderne, coloré qui est intuitif à la premère utilisation (ce n'est clairement pas le sujet pour un logiciel libre financé par les dons)

   - Protéger votre vie privée alors qu'il protège juste vos mots de passe (mais une fois adopté vous pouvez actver ensuite sur la suppression systématique de tous vos cookies de navigateur sans ennui ce qui est un grand pas en avant en terme de confidentialité !)

La sécurité viendra toujours s'opposer à la pratique. Tout est affaire de compromis. Le gain de sécurité est énorme dans le cas d'un gestionnaire de mot de passe hors-ligne accessible de partout comme KeePass. Peut être que pour vous, ces barrières ne valent pas la peine d'être franchies car oui, la sécurité ça ennuie plus que ça semble vous protéger. Mais trouverez vous ça réellement pratique d'avoir à récupérer/recrééer les comptes que vous avez perdu car ils utilisaient tous le même procédé de fabrication de mot de passe et que certains ont fuité ? 

# 4. Installation Logiciel de base

A télécharger sur keepass.info. Je recommande la version 2. Après installation du logiciel en anglais par défaut, 
une traduction française est diponible ici : https://keepass.info/translations.html

# 5. Sur ce dépôt Git :

* Vous trouverez des informations pour :
    * Installer Keepass
    * Installer le plugin Kee pour Firefox et comment l'utiliser
    * Synchroniser avec les changements réalisé sur votre ordinateur en local avec une base de données dans le cloud
    * Un script pour importer les mots de passe Firefox et les intégrer à Keepass
    * La double-authentification et sa gestion avec KeePass et Kee
    * Quelques plugins coup de coeurs


